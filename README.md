# cyber-books 

Jupyter notebooks from cybersecurity studying sessions.

1. [Introduction to encryption](crypto/intro_to_encryption.ipynb)
1. [The Art of Hacking (Secuirty Penetration Testing) Lessons 1 to 5](h4cker/spt_L1_5_the_art_of_hacking.ipynb)
1. [The Art of Hacking (Secuirty Penetration Testing) Lessons 6 to 10](h4cker/spt_L6_10_the_art_of_hacking.ipynb)
1. [The Art of Hacking (Hacking Web Applications)](h4cker/hwa_the_art_of_hacking.ipynb)

